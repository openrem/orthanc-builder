# Description

This repo contains build instructions for the following components:

- the osimis/orthanc Dockerhub image
- the osimis/orthanc-pro Dockerhub image
- the Windows installer
- the OSX package (zip with exe and plugins, contains the stable and unstable versions)
- the Windows package (zip with exe and plugins, contains the stable and unstable versions)

